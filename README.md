<!-- markdownlint-disable MD033 MD036 MD041 -->

_✨ And the game was over and the player woke up from the dream. And the player began a new dream. And the player dreamed again, dreamed better. And the player was the universe. And the player was love. ✨_

_✨ You're the player. Wake up. ✨_



<center><img src="https://q1.qlogo.cn/g?b=qq&nk=2119208499&s=4" alt="SEALchanPS's QQ avatar" style="zoom:50%;" /></center>

<center><font size=5>SEALchanPS's Self Intro!</font></center>

Hey there! I'm SEALchanPS, or 雾色深海_sc to come. I'm a 16-year-old senior high student currently residing in Xi'an, Shaanxi, China.

I'm a total tech nerd - I am almost crazy about everything related to technology. I code in Python (though not so proficient) and I'm currently picking up Rust.

When I'm not programming, you can find me studying or making videos on Bilibili and YouTube. I'm also really into playing MUG.

Great to meet you! If you wanna make friends with me, just FIRE AWAY! I'm an open book.

---

## Basic Information

![nickname](https://img.shields.io/badge/nickname-sc-green) ![GitLab User ID](https://img.shields.io/badge/GitLab%20User%20ID-15484920-green) ![Twitter Follow](https://img.shields.io/twitter/follow/SEALchanPS) 
